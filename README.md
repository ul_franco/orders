# Spring MVC + ExtJs 6.0.2 веб приложение #

### Запуск приложения ###

* mvn compile tomcat7:run
* Тестировалось на Apache Tomcat/7.0.47
* Приложение доступно по адресу http://localhost:8080/


### Работа с API ###

* Получение списка заказов

curl 'http://localhost:8080/orders?page=1&limit=5'

{"content":[{"id":0,"name":"Order 1","to":{"id":1,"name":"Contragent 1"},"from":{"id":2,"name":"Contragent 2"},"status":{"name":"Status 1"}},{"id":1,"name":"Order 2","to":{"id":2,"name":"Contragent 2"},"from":{"id":4,"name":"Contragent 4"},"status":{"name":"Status 4"}},{"id":2,"name":"Order 3","to":{"id":3,"name":"Contragent 3"},"from":{"id":5,"name":"Contragent 5"},"status":{"name":"Status 3"}},{"id":3,"name":"Order 4","to":{"id":4,"name":"Contragent 4"},"from":{"id":7,"name":"Contragent 7"},"status":{"name":"Status 4"}},{"id":4,"name":"Order 5","to":{"id":5,"name":"Contragent 5"},"from":{"id":8,"name":"Contragent 8"},"status":{"name":"Status 1"}}],"totalPages":3,"totalElements":15,"last":false,"numberOfElements":5,"sort":null,"first":true,"size":5,"number":0}

* Получение списка контрагентов

curl 'http://localhost:8080/contragents'

{"content":[{"id":1,"name":"Contragent 1"},{"id":2,"name":"Contragent 2"},{"id":3,"name":"Contragent 3"},{"id":4,"name":"Contragent 4"},{"id":5,"name":"Contragent 5"},{"id":6,"name":"Contragent 6"},{"id":7,"name":"Contragent 7"},{"id":8,"name":"Contragent 8"},{"id":9,"name":"Contragent 9"},{"id":10,"name":"Contragent 10"}]}

* Получение списка статусов

curl 'http://localhost:8080/statuses'

{"content":[{"name":"Status 1"},{"name":"Status 2"},{"name":"Status 3"},{"name":"Status 4"},{"name":"Status 5"}]}

* Удаление заказа с id=0
curl -X DELETE 'http://localhost:8080/orders/0'

* Добавление заказа

curl -X POST --data '{"id":null,"name":"11","from":{"id":10,"name":"Contragent 10"},"to":{"id":10,"name":"Contragent 10"},"status":{"name":"Status 5"}}'  -H "Content-Type: application/json" 'http://localhost:8080/orders'

{"id":15,"name":"11","to":{"id":10,"name":"Contragent 10"},"from":{"id":10,"name":"Contragent 10"},"status":{"name":"Status 5"}}

* Редактирование заказа

curl -X PUT --data '{"id":15,"name":"New Name","from":{"id":10,"name":"Contragent 10"},"to":{"id":10,"name":"Contragent 10"},"status":{"name":"Status 5"}}'  -H "Content-Type: application/json" 'http://localhost:8080/orders/15'

{"id":15,"name":"New Name","to":{"id":10,"name":"Contragent 10"},"from":{"id":10,"name":"Contragent 10"},"status":{"name":"Status 5"}}
