package ru.ibs.grasshopper.controller;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.ibs.grasshopper.config.JPAConfig;
import ru.ibs.grasshopper.model.TestConfig;

import java.io.File;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by grasshopper on 22.05.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfig.class, TestConfig.class})
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrdersControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private OrdersController controller;


    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void test1GetOrders() throws Exception {

        MockHttpServletRequestBuilder requestBuilder = get("/orders?page=1&limit=5");

        File file = new File("src/test/java/ru/ibs/grasshopper/controller/getOrdersResponse.json");
        String json = FileUtils.readFileToString(file);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json(json));

    }

    @Test
    public void test2DeleteOrder() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = delete("/orders/0");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }

    @Test
    public void test3CreateOrder() throws Exception {

        File file = new File("src/test/java/ru/ibs/grasshopper/controller/createOrderRequest.json");
        String jsonRequest = FileUtils.readFileToString(file);

        MockHttpServletRequestBuilder requestBuilder = post("/orders").
                content(jsonRequest).
                contentType(MediaType.APPLICATION_JSON_UTF8);

        file = new File("src/test/java/ru/ibs/grasshopper/controller/createOrderResponse.json");
        String jsonResponse = FileUtils.readFileToString(file);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json(jsonResponse));
    }

    @Test
    public void test4GetContragents() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/contragents");

        File file = new File("src/test/java/ru/ibs/grasshopper/controller/getContragentsResponse.json");
        String json = FileUtils.readFileToString(file);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json(json));
    }

    @Test
    public void test5GetStatuses() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = get("/statuses");

        File file = new File("src/test/java/ru/ibs/grasshopper/controller/getStatusesResponse.json");
        String json = FileUtils.readFileToString(file);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk()).
                andExpect(content().json(json));
    }
}