package ru.ibs.grasshopper.model;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by grasshopper on 22.05.16.
 */
@Configuration
@ComponentScan(basePackages = {"ru.ibs.grasshopper.model", "ru.ibs.grasshopper.controller"})
public class TestConfig extends WebMvcConfigurerAdapter {
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}
