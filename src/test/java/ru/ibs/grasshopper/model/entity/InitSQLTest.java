package ru.ibs.grasshopper.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ibs.grasshopper.config.JPAConfig;

import static org.junit.Assert.assertEquals;

/**
 * Created by grasshopper on 19.05.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfig.class})
public class InitSQLTest {

    @Autowired
    private ContragentRepository contragentRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testInitSql() {
        assertEquals(10, contragentRepository.count());
        assertEquals(5, statusRepository.count());
        assertEquals(15, orderRepository.count());

        Order o = orderRepository.findOne(1L);

        assertEquals("Order 2", o.getName());
        assertEquals("Contragent 2", o.getTo().getName());
        assertEquals("Contragent 4", o.getFrom().getName());
        assertEquals("Status 4", o.getStatus().getName());
    }

}