package ru.ibs.grasshopper.model;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.ibs.grasshopper.config.JPAConfig;
import ru.ibs.grasshopper.controller.OrdersLoadRequest;
import ru.ibs.grasshopper.model.entity.Contragent;
import ru.ibs.grasshopper.model.entity.Order;
import ru.ibs.grasshopper.model.entity.OrderRepository;
import ru.ibs.grasshopper.model.entity.Status;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by grasshopper on 22.05.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfig.class, TestConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrdersServiceTest {

    @Autowired
    private OrdersServiceIF service;
    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void test1GetOrders() {
        OrdersLoadRequest request = new OrdersLoadRequest();
        request.setLimit(5);
        request.setPage(1);

        Page<Order> response = service.getOrders(request);

        assertNotNull(response);
        assertEquals(0, response.getNumber());
        assertEquals(3, response.getTotalPages());
        assertEquals(15, response.getTotalElements());
        assertEquals(5, response.getNumberOfElements());
        assertNotNull(response.getContent());
        assertEquals(5, response.getContent().size());

        Order o = response.getContent().get(0);
        assertEquals(Long.valueOf(0), o.getId());
        assertEquals("Order 1", o.getName());

        Contragent from = o.getFrom();
        assertNotNull(from);
        assertEquals("Contragent 2", from.getName());
        assertEquals(Long.valueOf(2), from.getId());

        Contragent to = o.getTo();
        assertNotNull(to);
        assertEquals("Contragent 1", to.getName());
        assertEquals(Long.valueOf(1), to.getId());

        Status status = o.getStatus();
        assertNotNull(status);
        assertEquals("Status 1", status.getName());
    }

    public void test2DeleteOrder() {
        service.deleteOrder(0L);
        assertEquals(14, orderRepository.count());
    }

    public void test3Save() {
        Order o = orderRepository.findOne(1L);
        o.setName("New name");
        service.save(o);

        o = orderRepository.findOne(1L);
        assertEquals("New name", o.getName());
    }

    @Test
    public void test4GetContragents() {
        assertEquals(10, service.getContragents().getContent().size());
    }

    @Test
    public void test5GetStatuses() {
        assertEquals(5, service.getStatuses().getContent().size());
    }
}