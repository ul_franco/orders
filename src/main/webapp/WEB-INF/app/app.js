Ext.application({
  requires: ['Ext.container.Viewport'],
  name: 'APP',

  // automatically create an instance of APP.view.Viewport
  autoCreateViewport: true,

  models: ['Order', 'Contragent', 'Status'],
  stores: ['Orders', 'Contragents', 'Statuses'],
  views: ['Viewport', 'OrderList', 'OrderDetails', 'OrdersController'],
  mainView: 'App.view.Viewport'
});
