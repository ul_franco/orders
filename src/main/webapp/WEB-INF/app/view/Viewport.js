Ext.define('APP.view.Viewport', {
  extend: 'Ext.container.Container',

  layout: 'border',
  items: [{
    region: 'north',
    xtype: 'panel',
    title: 'Welcome to Orders application'
  }, {
    region: 'west',
    xtype: 'orderlist',
  }, {
    region: 'east',
    xtype: 'orderdetails'
  }]
});
