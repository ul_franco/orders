Ext.define('APP.view.OrderList', {
  extend: 'Ext.grid.Panel',

  alias: 'widget.orderlist',
  id: 'orderlist',

  controller: 'OrdersController',
  store: 'Orders',

  height: '70%',
  width: '50%',
  title: 'Order list',
  bodyStyle: 'padding: 5px 3px;',

  columns: [{
    text: 'ID',
    dataIndex: 'id'
  }, {
    text: 'Name',
    dataIndex: 'name',
    editor: {
      allowBlank: false
    },
    autoSizeColumn: true
  }, {
    text: 'From',
    dataIndex: 'from_name',
    autoSizeColumn: true,
    width: 150,
    editor: {
      xtype: 'combobox',
      store: 'Contragents',
      displayField: 'name',
      valueField: 'name',
      editable: false,
      queryMode: 'local',
      forceSelection: true,
      triggerAction: 'all',
      allowBlank: false,
      autoSelect: true,
      listeners: {
        select: 'comboSelect'
      }
    }
  }, {
    text: 'To',
    dataIndex: 'to_name',
    autoSizeColumn: true,
    width: 150,
    editor: {
      xtype: 'combobox',
      store: 'Contragents',
      displayField: 'name',
      valueField: 'name',
      editable: false,
      queryMode: 'local',
      forceSelection: true,
      triggerAction: 'all',
      allowBlank: false,
      autoSelect: true,
      listeners: {
        select: 'comboSelect'
      }
    }
  }, {
    text: 'Status',
    dataIndex: 'status_name',
    autoSizeColumn: true,
    editor: {
      xtype: 'combobox',
      store: 'Statuses',
      displayField: 'name',
      valueField: 'name',
      editable: false,
      queryMode: 'local',
      forceSelection: true,
      triggerAction: 'all',
      allowBlank: false,
      autoSelect: true,
      listeners: {
        select: 'comboSelect'
      }
    }
  }],


  selModel: {
    mode: 'SINGLE'
  },
  selType: 'checkboxmodel',

  plugins: [Ext.create('Ext.grid.plugin.RowEditing', {
    clicksToEdit: 2
  })],

  bbar: {
    xtype: 'pagingtoolbar',
    store: 'Orders',
    displayInfo: true,
    displayMsg: 'Displaying {0} to {1} of {2} &nbsp;records ',
    emptyMsg: 'No records to display&nbsp;'
  },

  dockedItems: [{
    xtype: 'toolbar',
    dock: 'bottom',
    ui: 'footer',
    layout: {
      pack: 'center'
    },
    defaults: {
      minWidth: 80
    },
    items: [{
      text: 'Create',
      itemId: 'btnCreate',
      listeners: {
        click: 'createOrder'
      }
    }, {
      text: 'Refresh Data',
      itemId: 'btnRefresh',
      listeners: {
        click: 'loadOrders'
      }
    }, {
      text: 'Save',
      itemId: 'btnSave',
      listeners: {
        click: 'save'
      }
    }, {
      text: 'Delete',
      itemId: 'btnDelete',
      listeners: {
        click: 'deleteOrder'
      }
    }]
  }],

  listeners: {
    select: 'selectRow'
  }

});
