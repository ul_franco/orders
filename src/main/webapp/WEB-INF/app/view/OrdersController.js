Ext.define('APP.view.OrdersController', {
  extend: 'Ext.app.ViewController',

  alias: 'controller.OrdersController',

  init: function(view) {
    this.loadOrders();
  },

  loadOrders: function() {
    var grid = this.getView();
    var store = grid.getStore();
    store.loadPage(1);
  },

  deleteOrder: function() {
    var grid = this.getView();
    var store = grid.getStore();

    var selection = grid.getSelectionModel().getSelection();

    if (selection.length) {
      this.selectNullRow();
      store.remove(selection);
    } else {
      Ext.Msg.alert('Warning', 'Please select an Order to delete!');
    }

  },

  save: function() {
    var grid = this.getView();
    var store = grid.getStore();

    store.sync({
      success: function(batch, eOpts) {
        Ext.Msg.alert('Success', 'Changes saved successfully.');
        store.loadPage(store.currentPage);
      },
      failure: function(batch, eOpts) {
        Ext.Msg.alert('Error', 'Request failed.');
      }
    });
    this.selectRow();
  },

  createOrder: function() {

    var grid = this.getView();
    var store = grid.getStore();

    var order = Ext.create('APP.model.Order');
    order.set('id', null);
    store.add(order);

    grid.editingPlugin.startEdit(order, 3);
  },

  comboSelect: function(editor, newObject, eOpts) {
    var dataIndex = editor.dataIndex;
    var grid = this.getView();
    var store = grid.getStore();
    var selection = grid.getSelectionModel().getSelection();

    if (dataIndex == "from_name") {
      if (selection[0].data.from != newObject.data) {
        selection[0].data.from = newObject.data;
        selection[0].dirty = true;
      }
      return;
    }

    if (dataIndex == "to_name") {
      if (selection[0].data.to != newObject.data) {
        selection[0].data.to = newObject.data;
        selection[0].dirty = true;
      }
      return;
    }

    if (dataIndex == "status_name") {
      if (selection[0].data.status != newObject.data) {
        selection[0].data.status = newObject.data;
        selection[0].dirty = true;
      }
      return;
    }
  },

  selectRow: function() {
    var grid = this.getView();
    var store = grid.getStore();
    var selection = grid.getSelectionModel().getSelection();
    var form = Ext.getCmp('orderdetails').form;
    if (selection.length) {
      form.loadRecord(selection[0]);
    } else {
      form.reset();
    }
  },

  selectNullRow: function() {
    var form = Ext.getCmp('orderdetails').form;
    form.reset();
  }

});
