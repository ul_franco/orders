Ext.define('APP.view.OrderDetails', {
  extend: 'Ext.form.Panel',

  title: 'Order details',
  width: '50%',

  bodyStyle: 'margin: 5px; padding: 5px 3px;',

  alias: 'widget.orderdetails',
  id: 'orderdetails',
  defaultType: 'textfield',

  items: [{
    fieldLabel: 'ID',
    name: 'id',
    disabled: true
  }, {
    fieldLabel: 'Name',
    name: 'name',
    disabled: true
  }, {
    fieldLabel: 'To',
    name: 'to_name',
    disabled: true
  }, {
    fieldLabel: 'From',
    name: 'from_name',
    disabled: true
  }, {
    fieldLabel: 'Status',
    name: 'status_name',
    disabled: true
  }]

});
