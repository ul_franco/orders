Ext.define('APP.model.Order', {
  extend: 'Ext.data.Model',
  idProperty: 'id',

  fields: [{
    name: 'id',
    type: 'int'
  }, {
    name: 'name',
    type: 'string'
  }, {
    name: 'from'
  }, {
    name: 'to'
  }, {
    name: 'status'
  }, {
    name: 'from_name',
    persist: false,
    type: 'string',
    calculate: function(o) {
      return typeof o.from != 'undefined' ? o.from.name : '';
    }
  }, {
    name: 'to_name',
    persist: false,
    type: 'string',
    calculate: function(o) {
      return typeof o.to != 'undefined' ? o.to.name : '';
    }
  }, {
    name: 'status_name',
    persist: false,
    type: 'string',
    calculate: function(o) {
      return typeof o.status != 'undefined' ? o.status.name : '';
    }
  }],

  associations: [{
    type: 'hasOne',
    model: 'Status',
    associationKey: 'status'
  }, {
    type: 'hasOne',
    model: 'Contragent',
    associationKey: 'to'
  }, {
    type: 'hasOne',
    model: 'Contragent',
    associationKey: 'from',
    getterName: 'getFrom',
    setterName: 'setFrom'
  }]

});
