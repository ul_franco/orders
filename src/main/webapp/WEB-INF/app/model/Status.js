Ext.define('APP.model.Status', {
  extend: 'Ext.data.Model',
  idProperty: 'name',
  fields: [{
    name: 'name',
    type: 'string'
  }]
});
