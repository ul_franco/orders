Ext.define('APP.store.Statuses', {
  extend: 'Ext.data.Store',

  model: 'APP.model.Status',
  autoLoad: true,

  proxy: {
    type: 'rest',
    startParam: false,
    noCache: false,
    pageParam: false,
    limitParam: false,
    url: '/statuses',
    reader: {
      type: 'json',
      rootProperty: 'content',
    }
  }
});
