  Ext.define('APP.store.Contragents', {
  extend: 'Ext.data.Store',

  model: 'APP.model.Contragent',
  autoLoad: true,

  proxy: {
    type: 'rest',
    startParam: false,
    noCache: false,
    pageParam: false,
    limitParam: false,
    url: '/contragents',
    reader: {
      type: 'json',
      rootProperty: 'content',
    }
  }
});
