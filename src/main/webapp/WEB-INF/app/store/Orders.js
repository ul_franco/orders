Ext.define('APP.store.Orders', {
  extend: 'Ext.data.Store',

  storeId: 'Orders',
  model: 'APP.model.Order',
  autoLoad: false,
  pageSize: 5,

  proxy: {
    type: 'rest',
    startParam: false,
    noCache: false,
    url: '/orders',
    reader: {
      type: 'json',
      rootProperty: 'content',
      totalProperty: 'totalElements'
    },
    writer: {
      writeAllFields: true,
      allowSingle: true
    }
  }
});
