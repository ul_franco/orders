<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Orders</title>

    <link rel="stylesheet" type="text/css" href="extjs/theme-neptune/resources/theme-neptune-all.css">
    <script type="text/javascript" src="extjs/ext-all.js"></script>
    <script type="text/javascript" src="extjs/theme-neptune/theme-neptune.js"></script>

    <script type="text/javascript" src="app/app.js"></script>
  </head>
  <body></body>
</html>
