insert into Contragent (id, name) values
(1, 'Contragent 1'),
(2, 'Contragent 2'),
(3, 'Contragent 3'),
(4, 'Contragent 4'),
(5, 'Contragent 5'),
(6, 'Contragent 6'),
(7, 'Contragent 7'),
(8, 'Contragent 8'),
(9, 'Contragent 9'),
(10, 'Contragent 10');

insert into Status (name) values
('Status 1'),
('Status 2'),
('Status 3'),
('Status 4'),
('Status 5');

insert into Orders(name, to_id, from_id, status_id) values
('Order 1', 1, 2, 'Status 1'),
('Order 2', 2, 4, 'Status 4'),
('Order 3', 3, 5, 'Status 3'),
('Order 4', 4, 7, 'Status 4'),
('Order 5', 5, 8, 'Status 1'),
('Order 6', 6, 9, 'Status 2'),
('Order 7', 7, 4, 'Status 3'),
('Order 8', 8, 1, 'Status 4'),
('Order 9', 9, 2, 'Status 5'),
('Order 10', 10, 1, 'Status 1'),
('Order 11', 1, 10, 'Status 2'),
('Order 12', 2, 5, 'Status 1'),
('Order 13', 2, 4, 'Status 3'),
('Order 14', 1, 3, 'Status 4'),
('Order 15', 1, 10, 'Status 5');

