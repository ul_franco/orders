drop table Contragent if exists;

create table Contragent(
id IDENTITY PRIMARY KEY,
name varchar(255) not null
);

drop table Status if exists;

create table Status(
name varchar(15) PRIMARY KEY
);

drop table Orders if exists;

create table Orders(
id IDENTITY PRIMARY KEY,
name varchar(255) not null,
to_id integer not null,
from_id integer not null,
status_id varchar(15) not null
);

alter table Orders add constraint FK_from_id foreign key (from_id) references Contragent;
alter table Orders add constraint FK_to_id foreign key (to_id) references Contragent;
alter table Orders add constraint FK_status_id foreign key (status_id) references Status;