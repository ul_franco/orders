package ru.ibs.grasshopper.model;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ibs.grasshopper.controller.ListResponse;
import ru.ibs.grasshopper.controller.OrdersLoadRequest;
import ru.ibs.grasshopper.model.entity.*;

/**
 * Created by grasshopper on 19.05.16.
 */
@Service
public class OrdersService implements OrdersServiceIF {

    private final ContragentRepository contragentRepository;
    private final StatusRepository statusRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public OrdersService(ContragentRepository contragentRepository, StatusRepository statusRepository, OrderRepository orderRepository) {
        this.contragentRepository = contragentRepository;
        this.statusRepository = statusRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public Page<Order> getOrders(OrdersLoadRequest request) {
        return orderRepository.findAll(new PageRequest(request.getPage() - 1, request.getLimit()));
    }

    @Override
    @Transactional
    public void deleteOrder(Long id) {
        orderRepository.delete(id);
    }

    @Override
    @Transactional
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public ListResponse<Contragent> getContragents() {
        return ListResponse.of(Lists.newArrayList(contragentRepository.findAll()));
    }

    @Override
    public ListResponse<Status> getStatuses() {
        return ListResponse.of(Lists.newArrayList(statusRepository.findAll()));
    }
}