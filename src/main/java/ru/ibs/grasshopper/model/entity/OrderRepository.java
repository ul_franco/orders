package ru.ibs.grasshopper.model.entity;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by grasshopper on 19.05.16.
 */
public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
}
