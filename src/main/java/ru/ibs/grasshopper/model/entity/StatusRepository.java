package ru.ibs.grasshopper.model.entity;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by grasshopper on 19.05.16.
 */
public interface StatusRepository extends CrudRepository<Status, String> {
}
