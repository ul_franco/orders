package ru.ibs.grasshopper.model;

import org.springframework.data.domain.Page;
import ru.ibs.grasshopper.controller.ListResponse;
import ru.ibs.grasshopper.controller.OrdersLoadRequest;
import ru.ibs.grasshopper.model.entity.Contragent;
import ru.ibs.grasshopper.model.entity.Order;
import ru.ibs.grasshopper.model.entity.Status;

/**
 * Created by grasshopper on 19.05.16.
 */
public interface OrdersServiceIF {

    Page<Order> getOrders(OrdersLoadRequest request);

    void deleteOrder(Long id);

    Order save(Order order);

    ListResponse<Contragent> getContragents();

    ListResponse<Status> getStatuses();
}
