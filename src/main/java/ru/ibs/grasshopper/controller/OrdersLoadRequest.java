package ru.ibs.grasshopper.controller;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by grasshopper on 20.05.16.
 */
public class OrdersLoadRequest {
    @NotNull
    @Min(1)
    private Integer page;
    @NotNull
    @Min(1)
    private Integer limit;

    public OrdersLoadRequest() {
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "OrdersLoadRequest{" +
                "page=" + page +
                ", limit=" + limit +
                '}';
    }
}
