package ru.ibs.grasshopper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ibs.grasshopper.model.OrdersServiceIF;
import ru.ibs.grasshopper.model.entity.Order;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@CrossOrigin
public class OrdersController {

    private final OrdersServiceIF ordersService;

    @Autowired
    public OrdersController(OrdersServiceIF ordersService) {
        this.ordersService = ordersService;
    }

    @RequestMapping(value = "/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/orders", method = GET)
    public Object getOrders(@Valid @ModelAttribute OrdersLoadRequest r, BindingResult result) {
        System.out.println("<< getOrders. received:\n" + r);
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ordersService.getOrders(r);
    }

    @RequestMapping(value = "/orders/{id}", method = DELETE)
    public void deleteOrder(@PathVariable("id") Long id) {
        System.out.println("<< deleteOrder. id = " + id);
        ordersService.deleteOrder(id);
    }

    @RequestMapping(value = "/orders", method = POST)
    public Order createOrder(@RequestBody Order order) {
        System.out.println("<< createOrder. order = " + order.toFullString());
        order.setId(null);
        return ordersService.save(order);
    }

    @RequestMapping(value = "/orders/{id}", method = PUT)
    public Order editOrder(@RequestBody Order order, @PathVariable("id") Long id) {
        System.out.println("<< editOrder. order = " + order.toFullString() + ". id = " + id);
        return ordersService.save(order);
    }

    @RequestMapping(value = "/contragents", method = GET)
    public ListResponse getContragents() {
        System.out.println("<< getContragents");
        return ordersService.getContragents();
    }

    @RequestMapping(value = "/statuses", method = GET)
    public ListResponse getStatuses() {
        System.out.println("<< getStatuses");
        return ordersService.getStatuses();
    }
}