package ru.ibs.grasshopper.controller;

import java.util.List;

/**
 * Created by grasshopper on 21.05.16.
 */
public class ListResponse<T> {
    private List<T> content;

    protected ListResponse(List<T> content) {
        this.content = content;
    }

    public static ListResponse of(List content) {
        return new ListResponse<>(content);
    }

    public List<T> getContent() {
        return content;
    }
}
